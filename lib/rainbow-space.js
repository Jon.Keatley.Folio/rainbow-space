'use babel';
    
import { CompositeDisposable, Point } from 'atom';
      
export default {
  subscriptions: null,
  isEnabled: false,

  currentEditor: undefined,
  maxRange:5,
  decorations: new Set(),
    
  decorateLine(self,editor,row) {
    const line = editor.getBuffer().getLines()[row];
    const ws = editor.getTabText();
    const lengthInWhiteSpace = Math.floor(line.length / ws.length)

    for(var x =0; x<lengthInWhiteSpace;x+= ws.length)
    {

      if(line.slice(x,x + ws.length) === ws) {
        marker = editor.markBufferRange([[row,x],[row,x + ws.length]],{invalidate:'touch'});
        var index = x;
        while(index > this.maxRange)
        {
          index -= this.maxRange;
        }
        self.decorations.add(editor.decorateMarker(marker,{type: 'text', class:'rainbowspace' + index}));
      }
      else {
        break;
      }
    } 
  },
           
  updateRainbow(self,editor) {
    if(self.isEnabled === true) {
        var buffer = editor.getBuffer();
        for (var row = 0; row < buffer.getLines().length;row++) {
          self.decorateLine(self,editor,row)
        } 
     }
  },
                
  updateRainbowSection(self,editor,range,txt) {
    if(self.isEnabled === true) {
      for (var row=range.start.row;row <= range.end.row;row++)
      {
        self.decorateLine(self,editor,row);
      }
    }
  },

  activate(state) {
    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();
    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'rainbow-space:toggle': () => this.toggle()
    }));

    this.subscriptions.add(atom.workspace.observeActiveTextEditor(editor => {
      this.activateEditor(this,editor);
    }));
  },
   
  deactivate() {
    this.subscriptions.dispose();
  },
     
  toggle() {
    console.log('RainbowSpace was toggled!');
    this.isEnabled = !this.isEnabled;
    if(this.isEnabled && this.currentEditor)
    {
      this.updateRainbow(this,this.currentEditor);
    }
    else {
      this.clearDecorations();
    }
    
    return ( this.isEnabled );
  },
  
  activateEditor(self,editor) {
    self.currentEditor = editor;
    if(editor) {
      self.clearDecorations();
      this.subscriptions.add(editor.onDidStopChanging(changes => 
        {
          this.onEditorChanged(this,changes.changes,editor)
        }
      ))
      self.subscriptions.add(editor.onDidDestroy(() => {this.onEditorDistroyed(this,editor)}));
      self.updateRainbow(this,editor);
    }
  },
    
  onEditorChanged(self,changes,editor) {
    for(var x=0; x< changes.length;x++)
    {
      self.updateRainbowSection(self,editor,changes[x].newRange,changes[x].newText);
    }
  },

  onEditorDistroyed(self,editor) {
    self.clearDecorations();
    self.activateEditor(self,atom.workspace.getActiveTextEditor());
  },
  
  clearDecorations() {
    for (const dec of this.decorations) {
      dec.destroy();
    }
    this.decorations = new Set();
  }
};
