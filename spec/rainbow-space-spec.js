'use babel';

import RainbowSpace from '../lib/rainbow-space';

// Use the command `window:run-package-specs` (cmd-alt-ctrl-p) to run specs.
//
// To run a specific `it` or `describe` block add an `f` to the front (e.g. `fit`
// or `fdescribe`). Remove the `f` to unfocus the block.

describe('RainbowSpace', () => {
  let workspaceElement, activationPromise;

  beforeEach(() => {
    workspaceElement = atom.views.getView(atom.workspace);
    activationPromise = atom.packages.activatePackage('rainbow-space');
  });

  describe('when the rainbow-space:toggle event is triggered', () => {
    it('hides and shows the modal panel', () => {
      // Before the activation event the view is not on the DOM, and no panel
      // has been created
      expect(workspaceElement.querySelector('.rainbow-space')).not.toExist();

      // This is an activation event, triggering it will cause the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'rainbow-space:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        expect(workspaceElement.querySelector('.rainbow-space')).toExist();

        let rainbowSpaceElement = workspaceElement.querySelector('.rainbow-space');
        expect(rainbowSpaceElement).toExist();

        let rainbowSpacePanel = atom.workspace.panelForItem(rainbowSpaceElement);
        expect(rainbowSpacePanel.isVisible()).toBe(true);
        atom.commands.dispatch(workspaceElement, 'rainbow-space:toggle');
        expect(rainbowSpacePanel.isVisible()).toBe(false);
      });
    });

    it('hides and shows the view', () => {
      // This test shows you an integration test testing at the view level.

      // Attaching the workspaceElement to the DOM is required to allow the
      // `toBeVisible()` matchers to work. Anything testing visibility or focus
      // requires that the workspaceElement is on the DOM. Tests that attach the
      // workspaceElement to the DOM are generally slower than those off DOM.
      jasmine.attachToDOM(workspaceElement);

      expect(workspaceElement.querySelector('.rainbow-space')).not.toExist();

      // This is an activation event, triggering it causes the package to be
      // activated.
      atom.commands.dispatch(workspaceElement, 'rainbow-space:toggle');

      waitsForPromise(() => {
        return activationPromise;
      });

      runs(() => {
        // Now we can test for view visibility
        let rainbowSpaceElement = workspaceElement.querySelector('.rainbow-space');
        expect(rainbowSpaceElement).toBeVisible();
        atom.commands.dispatch(workspaceElement, 'rainbow-space:toggle');
        expect(rainbowSpaceElement).not.toBeVisible();
      });
    });
  });
});
